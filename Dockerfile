FROM node:14

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

ENV PORT = 8070

EXPOSE 4000

CMD ["npm", "start"]
