const axios = require('axios');
require('dotenv').config();
const { v1: uuid } = require('uuid');
const Movie = require('../models/movie');

const nowPlayingAPI = process.env.NOW_PLAYING;

const playingNow = (req) => axios
  .get(nowPlayingAPI, {
    params: { api_key: process.env.API_KEY },
  })
  .then((response) => {
    const resData = response.data.results;
    // console.log(nowPlaying);
    req.logger.info(
      `API call:  req correlation id : ${req.correlationId} - response status: ${response.status}  - method ${req.method}`,
    );
    return resData;
    //   res.render("index", { resData });
  });

const topRatedMovies = (req) => axios
  .get(process.env.TOP_RATED, {
    params: { api_key: process.env.API_KEY },
  })
  .then((response) => {
    const ratedMovies = response.data.results;
    const rated = ratedMovies.filter((movie) => movie.vote_average > 8.5);
    req.logger.info(
      `API call:  req correlation id : ${req.correlationId} - response status: ${response.status}  - method ${req.method}`,
    );
    // console.log(rated);
    return rated;
  });

const movieSearch = (name, req) => axios
  .get(process.env.MOVIE_URL, {
    params: {
      api_key: process.env.API_KEY,
      query: name,
    },
    headers: {
      correlationId: uuid(),
    },
  })
  .then((response) => {
    const movieResults = response.data.results;
    req.logger.info(
      `API call:  req correlation id : ${req.correlationId} - response status: ${response.status}  - method ${req.method}`,
    );

    const titles = movieResults.map((movie) => movie.title);

    return Movie.create({ searchTerm: name, titles }).then((resdata) => {
      req.logger.info(
        `API call:  req correlation id : ${req.correlationId} - response status: ${response.status}  - method ${req.method}`,
      );
      console.log('Data inserted', name, titles); // Success
      return resdata;
    });
  });

const findMovie = (nameReq, req) => Movie.findOne({ searchTerm: nameReq }).then((docs) => {
  req.logger.info(
    `API call:  req correlation id : ${req.correlationId} - response object-ID xxx - method ${req.method}`,
  );

  if (!docs) {
    console.log('result: did not find name searched for', nameReq, docs);
    return `/movies/movie-search/${nameReq}`;
  }
  return docs.titles;
});

module.exports = {
  playingNow,
  topRatedMovies,
  movieSearch,
  findMovie,
};
