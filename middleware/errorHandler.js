module.exports = (error, req, res, next) => {
  // eslint-disable-line no-unused-vars
  // console.log("logging the error handler", error.message, error.stack);
  res.status(500).send({ error: error.message });
  req.logger.error(
    `error status: ${error.status || 500} - ${res.statusMessage} - ${
      error.message
    } - ${req.originalUrl} - ${req.method} - correlation Id: ${
      req.correlationId
    }`,
  );
  next();
};
