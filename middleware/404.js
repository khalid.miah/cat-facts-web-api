module.exports = (req, res, next) => {
  console.log('logging the error from not found error 404');
  res.status(404).send({ error: 'Page not found' });
  req.logger.error(
    `status code: || status-message: ${res.statusMessage} - url: ${req.originalUrl} - method: ${req.method} - correlation Id: ${req.correlationId}`,
  );
  next();
};
