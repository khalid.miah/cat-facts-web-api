const { createLogger, format, transports } = require('winston');
const { v1: uuid } = require('uuid');

const options = {
  transports: new transports.File({
    filename: 'logs/server.log',
    format: format.combine(
      format.timestamp({ format: 'MMM-DD-YYYY HH:mm:ss' }),
      format.align(),
      format.printf(
        (info) => `${info.level}: ${[info.timestamp]}: ${info.message}`,
      ),
    ),
  }),
};

const logger = createLogger(options);

module.exports = (req, res, next) => {
  const correlationId = req.get('correlation-id') || uuid();
  // console.log("correlation id or uuid = ", correlationId);

  req.correlationId = correlationId;
  // console.log("req correlation", req.correlationId);
  req.logger = logger;

  res.setHeader('correlation-id', correlationId);
  // console.log("find correlation id in res header", res.get("correlation-id"));

  next();
};

module.exports.options = options;
