const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose');

const app = express();
const path = require('path');
const movieRoute = require('./routes/movieRoutes');
const notFound = require('./middleware/404');
const errorHandler = require('./middleware/errorHandler');
const logger = require('./middleware/logger');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger);

// app.use(express.static(path.join(__dirname, "public")));

mongoose
  .connect(process.env.MONGO_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('connected to DB');
  })
  .catch((err) => {
    console.log('did not connect to DB');
    console.log(err);
  });

app.use('/movies', movieRoute);
app.use('/*', notFound);
app.use(errorHandler);

module.exports = app;
