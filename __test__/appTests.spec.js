const request = require('supertest');
const axios = require('axios');
const { logger } = require('winston');

const Movie = require('../models/movie');
const app = require('../index');

jest.mock('axios');
jest.mock('../models/movie');

describe('/ movie end points', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('returns with status 200', () => {
    axios.get.mockImplementation(() => Promise.resolve({ data: {} }));
    return request(app)
      .get('/movies/nowplaying')
      .set('Accept', 'application/json')
      .set('correlation-id', 'test_correlation')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .then((response) => {
        expect(response.status).toEqual(200);
        expect(logger.info).toHaveBeenCalledTimes(2);
        expect(logger.info).toHaveBeenNthCalledWith(
          1,
          'API call:  req correlation id : test_correlation - response status: undefined  - method GET',
        );
        expect(logger.info).toHaveBeenNthCalledWith(
          2,
          ' req correlation id : test_correlation - response status: 200 - url: /movies/nowplaying - method GET - response correlation id :test_correlation',
        );
      });
  });

  // it('returns names of movies ', async () => {
  //   const mOne = {
  //     id: 1,
  //     title: 'cats',
  //   };
  //   const mTwo = {
  //   id: 2,
  //   title: 'home alone',
  // };
  // const mThree = {
  //   id: 3,
  //   title: 'The Matrix',
  // };

  // const movieListAll = [mOne, mTwo, mThree];

  // axios.get.mockImplementation(() => Promise.resolve({ data: { results: movieListAll } }));
  //   return (
  //     request(app)
  //       .get('/movies/movie/2')
  //       // .set('Accept', 'application/json')
  //       // .expect('Content-Type', 'application/json; charset=utf-8')
  //       .then((response) => {
  //         // console.log(response.body);
  //         expect(response.body).toEqual(mTwo);
  //       })
  //   );
  // });
  it('returns movies that have vote average higher than 8 ', () => {
    const mOne = {
      id: 1,
      title: 'home alone 2',
      vote_average: '8.6',
    };
    const mTwo = {
      id: 2,
      title: 'home alone 1',
      vote_average: '9.5',
    };
    const mThree = {
      id: 3,
      title: 'The Matrix',
      vote_average: '7.6',
    };

    const movieListAll = [mOne, mTwo, mThree];

    axios.get.mockImplementation(() => Promise.resolve({ data: { results: movieListAll } }));
    return request(app)
      .get('/movies/top_rated')
      .set('Accept', 'application/json')
      .set('correlation-id', 'test_correlation')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .then((response) => {
        // console.log(response.body);
        expect(response.body).toEqual([mOne, mTwo]);
        expect(logger.info).toHaveBeenCalledTimes(2);
        expect(logger.info).toHaveBeenNthCalledWith(
          1,
          'API call:  req correlation id : test_correlation - response status: undefined  - method GET',
        );
        expect(logger.info).toHaveBeenNthCalledWith(
          2,
          'req correlation id : test_correlation - response status: OK - url: /movies/top_rated - method GET - response correlation id :test_correlation',
        );
      });
  });
  it('searches for movies with the name searched for  and stores in db', () => {
    const movieList = [
      'Spider-Man: No Way Home',
      'Home Sweet Home Alone',
      'Home',
    ];

    Movie.create.mockImplementation(() => Promise.resolve({ titles: movieList }));
    return request(app)
      .get('/movies/movie-search/home')
      .set('correlation-id', 'test_correlation')
      .then((response) => {
        console.log(response.body);
        expect(response.body).toEqual({ titles: movieList });
      });
  });
});

it('returns/finds  movies that was searched for ', () => {
  const movieList = [
    'Spider-Man: No Way Home',
    'Home Sweet Home Alone',
    'Home',
  ];

  Movie.findOne.mockImplementation(() => Promise.resolve({ titles: movieList }));

  return (
    request(app)
      .get('/movies/findMovie/home')
      .set('correlation-id', 'test_correlation')

      // .set("Accept", "application/json")
      // .expect("Content-Type", "application/json; charset=utf-8")
      .then((response) => {
        expect(response.body).toEqual(movieList);
        expect(logger.info).toHaveBeenCalledTimes(2);
        expect(logger.info).toHaveBeenNthCalledWith(
          1,
          'API call:  req correlation id : test_correlation - response object-ID xxx - method GET',
        );
        expect(logger.info).toHaveBeenNthCalledWith(
          2,
          'req correlation id : test_correlation - response status: undefined - url: /movies/findMovie/home - method GET - response correlation id :test_correlation',
        );
      })
  );
});

describe('GET error when url or server error', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('returns with status 404', () => request(app)
    .get('/movies/somethingWierd')
    .set('correlation-id', 'test_correlation')
    .then((response) => {
      expect(response.status).toEqual(404);
      // console.log(response.body);
      expect(response.body).toEqual({ error: 'Page not found' });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenNthCalledWith(
        1,
        'status code: || status-message: Not Found - url: /movies/somethingWierd - method: GET - correlation Id: test_correlation',
      );
    }));

  it('responds with status 500 for sever error', () => {
    axios.get.mockImplementation(() => Promise.reject(new Error('an error')));
    return request(app)
      .get('/movies/nowplaying')
      .set('correlation-id', 'test_correlation')
      .then((response) => {
        expect(response.status).toEqual(500);
        expect(response.body).toEqual({ error: 'an error' });
        expect(logger.error).toHaveBeenCalledTimes(1);
        expect(logger.error).toHaveBeenNthCalledWith(
          1,
          'error status: 500 - Internal Server Error - an error - /movies/nowplaying - GET - correlation Id: test_correlation',
        );
      });
  });
  it('responds with status 500 for db error', () => {
    Movie.findOne.mockImplementation(() => Promise.reject(new Error('an error')));
    return request(app)
      .get('/movies/findMovie/home')
      .set('correlation-id', 'test_correlation')
      .then((response) => {
        console.log('got the error body', response.body);
        expect(response.status).toEqual(500);
        expect(response.body).toEqual({ error: 'an error' });
        expect(logger.error).toHaveBeenCalledTimes(1);
        expect(logger.error).toHaveBeenNthCalledWith(
          1,
          'error status: 500 - Internal Server Error - an error - /movies/findMovie/home - GET - correlation Id: test_correlation',
        );
      });
  });
  it('responds with status 500 for db error', () => {
    Movie.create.mockImplementation(() => Promise.reject(new Error('an error')));
    return request(app)
      .get('/movies/movie-search/home')
      .set('correlation-id', 'test_correlation')
      .then((response) => {
        console.log('create response error', response.body);
        expect(response.status).toEqual(500);
        expect(response.body).toEqual({ error: 'an error' });
        expect(logger.error).toHaveBeenCalledTimes(1);
        expect(logger.error).toHaveBeenNthCalledWith(
          1,
          'error status: 500 - Internal Server Error - an error - /movies/movie-search/home - GET - correlation Id: test_correlation',
        );
      });
  });
});
