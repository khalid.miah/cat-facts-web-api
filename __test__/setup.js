jest.mock('mongoose', () => {
  const connect = jest.fn(() => Promise.resolve());
  const Schema = jest.fn();
  const model = jest.fn(() => ({
    findOne: jest.fn(),
    create: jest.fn(),
  }));
  return { connect, Schema, model };
});

jest.mock('winston', () => {
  const transports = {
    File: jest.fn(),
  };
  const format = {
    combine: jest.fn(),
    timestamp: jest.fn(),
    align: jest.fn(),
    printf: jest.fn(),
  };
  const logger = {
    info: jest.fn(),
    error: jest.fn(),
  };
  const createLogger = () => logger;
  return {
    logger,
    transports,
    format,
    createLogger,
  };
});
