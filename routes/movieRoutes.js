const express = require('express');

const router = express.Router();

const {
  playingNow,
  topRatedMovies,
  movieSearch,
  findMovie,
} = require('../controller/controller');

router.get('/nowplaying', (req, res, next) => {
  playingNow(req)
    .then((resData) => {
      res.json(resData);
      // console.log('req correlation id:', req.correlationId);
      // console.log('response', res.get('correlation-id'));
      req.logger.info(
        ` req correlation id : ${req.correlationId} - response status: ${
          res.statusCode
        } - url: ${req.originalUrl} - method ${
          req.method
        } - response correlation id :${res.get('correlation-id')}`,
      );
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/top_rated', (req, res, next) => {
  topRatedMovies(req)
    .then((resRated) => {
      console.log(resRated);
      res.send(resRated);
      req.logger.info(
        `req correlation id : ${req.correlationId} - response status: ${
          res.statusMessage
        } - url: ${req.originalUrl} - method ${
          req.method
        } - response correlation id :${res.get('correlation-id')}`,
      );
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/movie-search/:title', (req, res, next) => {
  const name = encodeURI(req.params.title);
  console.log(name);
  movieSearch(name, req)
    .then((result) => {
      // console.log(result);
      res.send(result);
      req.logger.info(
        `req correlation id : ${req.correlationId} - response status: ${
          res.statusMessage
        } - url: ${req.originalUrl} - method ${
          req.method
        } - response correlation id :${res.get('correlation-id')}`,
      );
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/findMovie/:name', (req, res, next) => {
  console.log(req.params.name);
  const nameReq = req.params.name;
  findMovie(nameReq, req)
    .then((result) => {
      // console.log(result);
      req.logger.info(
        `req correlation id : ${req.correlationId} - response status: ${
          result.statusCode
        } - url: ${req.originalUrl} - method ${
          req.method
        } - response correlation id :${res.get('correlation-id')}`,
      );
      if (typeof result === 'object') {
        res.json(result);
      } else {
        res.redirect(result);
      }
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
