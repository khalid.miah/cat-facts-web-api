const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
  searchTerm: {
    type: String,
  },
  titles: {},
});

const Movie = mongoose.model('Movie', movieSchema);

module.exports = Movie;
